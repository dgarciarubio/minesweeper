﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Minesweeper.Test
{
    public class GameTests
    {
        [Theory]
        [InlineData(1, 1, 0, 0)]
        [InlineData(1, 1, 1, 0)]
        [InlineData(3, 2, 3, 0)]
        [InlineData(2, 3, 6, 0)]
        public void RandomConstructorShouldCreateProperGame(int width, int height, int mineCount, int seed)
        {
            var random = new Random(seed);
            var sut = new Game(width, height, mineCount, random);

            AssertNewGameHasProperties(sut,
                expectedWidth: width,
                expectedHeight: height,
                expectedMineCount: mineCount);
        }

        [Fact]
        public void RandomConstructorShouldThrowIfInvalidParameters()
        {
            int validWidth = 1, invalidWidth = 0;
            int validHeight = 1, invalidHeight = 0;
            int validMineCount = 0, invalidMineCount = -1;
            int excessiveMineCount = validWidth * validHeight + 1;

            var random = new Random(0);

            Assert.Throws<ArgumentOutOfRangeException>(() => new Game(invalidWidth, validHeight, validMineCount, random));
            Assert.Throws<ArgumentOutOfRangeException>(() => new Game(validWidth, invalidHeight, validMineCount, random));
            Assert.Throws<ArgumentOutOfRangeException>(() => new Game(validWidth, validHeight, invalidMineCount, random));
            Assert.Throws<ArgumentException>(() => new Game(validWidth, validHeight, excessiveMineCount, random));
            Assert.Throws<ArgumentNullException>(() => new Game(validWidth, validHeight, validMineCount, null));
        }

        [Theory]
        [InlineData(1, 1)]
        [InlineData(3, 2)]
        [InlineData(2, 3)]
        public void DeterministicConstructorShouldCreateProperGame(int width, int height)
        {
            var random = new Random(0);
            BlockCoordinates mineCoordinates = new BlockCoordinates(
                x: random.Next(width),
                y: random.Next(height));
            var sut = new Game(width, height, new BlockCoordinates[] { mineCoordinates });

            AssertNewGameHasProperties(sut,
                expectedWidth: width,
                expectedHeight: height,
                expectedMineCount: 1);

            BlockState blockStateAfterResolve = sut.Reveal(mineCoordinates);

            Assert.True(blockStateAfterResolve.IsRevealed);
            Assert.True(blockStateAfterResolve.HasMine);
            Assert.Equal(Game.GameStatus.Defeat, sut.Status);
        }

        [Fact]
        public void DeterministicConstructorShouldThrowIfInvalidParameters()
        {
            int validWidth = 1, invalidWidth = 0;
            int validHeight = 1, invalidHeight = 0;
            IEnumerable<BlockCoordinates> validMineCoordinates = new BlockCoordinates[] { new BlockCoordinates(0, 0) };
            IEnumerable<BlockCoordinates> invalidMineCoordinates = new  BlockCoordinates[] { new BlockCoordinates(0, 1) };

            Assert.Throws<ArgumentOutOfRangeException>(() => new Game(invalidWidth, validHeight, validMineCoordinates));
            Assert.Throws<ArgumentOutOfRangeException>(() => new Game(validWidth, invalidHeight, validMineCoordinates));
            Assert.Throws<ArgumentException>(() => new Game(validWidth, validHeight, invalidMineCoordinates));
            Assert.Throws<ArgumentNullException>(() => new Game(validWidth, validHeight, null));
        }

        private static void AssertNewGameHasProperties(Game sut, int expectedWidth, int expectedHeight, int expectedMineCount)
        {
            int expectedSize = expectedWidth * expectedHeight;
            int expectedFlagCount = 0;

            Game.GameStatus expectedStatus = Game.GameStatus.InProgress;

            IEnumerable<BlockCoordinates> expectedBlockCoordinates = Enumerable.Range(0, expectedWidth)
                .SelectMany(x =>
                {
                    return Enumerable.Range(0, expectedHeight)
                        .Select(y => new BlockCoordinates(x, y));
                });

            IEnumerable<BlockState> expectedBlockStates = Enumerable.Range(0, expectedSize)
                .Select(i => default(BlockState));

            var sutDict = sut as IReadOnlyDictionary<BlockCoordinates, BlockState>;

            Assert.Equal(expectedWidth, sut.Width);
            Assert.Equal(expectedHeight, sut.Height);
            Assert.Equal(expectedSize, sut.Size);
            Assert.Equal(expectedSize, sutDict.Count);
            Assert.Equal(expectedMineCount, sut.MineCount);
            Assert.Equal(expectedFlagCount, sut.FlagCount);
            Assert.Equal(expectedStatus, sut.Status);
            Assert.Equal(expectedBlockCoordinates.OrderBy(c => c.X).ThenBy(c => c.Y), sut.BlockCoordinates.OrderBy(c => c.X).ThenBy(c => c.Y));
            Assert.Equal(expectedBlockCoordinates.OrderBy(c => c.X).ThenBy(c => c.Y), sutDict.Keys.OrderBy(c => c.X).ThenBy(c => c.Y));
            Assert.Equal(expectedBlockStates, sut.BlockStates);
            Assert.Equal(expectedBlockStates, sutDict.Values);
        }
    }
}

