using System;
using System.Globalization;
using Xunit;

namespace Minesweeper.Test
{
    public class BlockCoordinatesTests
    {
        [Fact]
        public void DefaultValueShouldBeZero()
        {
            int expectedX = 0;
            int expectedY = 0;
            BlockCoordinates sut = default;

            Assert.Equal(expectedX, sut.X);
            Assert.Equal(expectedY, sut.Y);
        }

        [Fact]
        public void ConstructorShouldCreatePropperCoordinates()
        {
            int x = 1, expectedX = x;
            int y = 1, expectedY = y;
            var sut = new BlockCoordinates(x, y);

            Assert.Equal(expectedX, sut.X);
            Assert.Equal(expectedY, sut.Y);
        }

        [Fact]
        public void ConstructorShouldThrowIfNegativeCoordinates()
        {
            int validX = 0, invalidX = -1;
            int validY = 0, invalidY = -1;

            Assert.Throws<ArgumentOutOfRangeException>(() => new BlockCoordinates(invalidX, validY));
            Assert.Throws<ArgumentOutOfRangeException>(() => new BlockCoordinates(validX, invalidY));
            Assert.Throws<ArgumentOutOfRangeException>(() => new BlockCoordinates(invalidX, invalidY));
        }

        [Fact]
        public void EqualCoordinatesMustBeEqual()
        {
            int x = 1;
            int y = 2;
            var sut1 = new BlockCoordinates(x, y);
            var sut2 = new BlockCoordinates(x, y);

            AssertEqualty(sut1, sut2);
        }

        [Fact]
        public void DifferentCoordinatesMustNotBeEqual()
        {
            int x1 = 1, x2 = 2;
            int y1 = 1, y2 = 2;
            var suts = new BlockCoordinates[]
            {
                new BlockCoordinates(x1, y1),
                new BlockCoordinates(x1, y2),
                new BlockCoordinates(x2, y1),
                new BlockCoordinates(x2, y2),
            };

            for(int i = 0; i < suts.Length; i++)
            {
                BlockCoordinates sut1 = suts[i]; 
                for (int j = 0; j < suts.Length; j++)
                {
                    if (i != j)
                    {
                        BlockCoordinates sut2 = suts[j];

                        AssertInequalty(sut1, sut2);
                    }
                }
            }
        }

        private void AssertEqualty(BlockCoordinates sut1, BlockCoordinates sut2)
        {
            Assert.True(sut1.Equals(sut2));
            Assert.True(sut1.Equals((object)sut2));
            Assert.True(sut1 == sut2);
            Assert.False(sut1 != sut2);

            Assert.Equal(sut1.ToString(), sut2.ToString());
            Assert.Equal(sut1.ToString(CultureInfo.CurrentCulture), sut2.ToString(CultureInfo.CurrentCulture));
            Assert.Equal(sut1.ToString("N"), sut2.ToString("N"));
            Assert.Equal(sut1.ToString("N", CultureInfo.CurrentCulture), sut2.ToString("N", CultureInfo.CurrentCulture));
            Assert.Equal(sut1.GetHashCode(), sut2.GetHashCode());
        }
        
        private void AssertInequalty(BlockCoordinates sut1, BlockCoordinates sut2)
        {
            Assert.False(sut1.Equals(sut2));
            Assert.False(sut1.Equals((object)sut2));
            Assert.False(sut1 == sut2);
            Assert.True(sut1 != sut2);

            Assert.NotEqual(sut1.ToString(), sut2.ToString());
            Assert.NotEqual(sut1.ToString(CultureInfo.CurrentCulture), sut2.ToString(CultureInfo.CurrentCulture));
            Assert.NotEqual(sut1.ToString("N"), sut2.ToString("N"));
            Assert.NotEqual(sut1.ToString("N", CultureInfo.CurrentCulture), sut2.ToString("N", CultureInfo.CurrentCulture));
        }

    }
}

