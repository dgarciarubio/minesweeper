﻿namespace Minesweeper
{
    /// <summary>
    /// Represents the state of a block in the Game.
    /// </summary>
    public struct BlockState
    {
        private BlockState(bool isRevealed = false, bool hasFlag = false, bool? hasMine = null, int? surroundingMineCount = null)
        {
            IsRevealed = isRevealed;
            HasFlag = hasFlag;
            HasMine = hasMine;
            SurroundingMineCount = surroundingMineCount;
        }

        /// <summary>
        /// Gets a value indicating whether the block is revealed or not.
        /// </summary>
        public bool IsRevealed { get; }
        /// <summary>
        /// Gets a value indicating whether the block has a flag or not.
        /// </summary>
        public bool HasFlag { get; }
        /// <summary>
        /// Gets a value indicating whether the block has a mine or not, or null if it is not revealed.
        /// </summary>
        public bool? HasMine { get; }
        /// <summary>
        /// Gets the count of mines in the surrounding blocks, or null if it is not revealed.
        /// </summary>
        public int? SurroundingMineCount { get; }

        internal static BlockState NonRevealed (bool hasFlag)
        {
            return new BlockState(isRevealed: false, hasFlag);
        }

        internal static BlockState RevealedWithMine(bool hasFlag)
        {
            return new BlockState(isRevealed: true, hasFlag, hasMine: true);
        }

        internal static BlockState RevealedWithNoMine(bool hasFlag, int surroundingMineCount)
        {
            return new BlockState(isRevealed: true, hasFlag, hasMine: false, surroundingMineCount);
        }
    }
}