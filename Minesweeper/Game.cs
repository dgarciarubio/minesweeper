﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Minesweeper
{
    /// <summary>
    /// Represents an instance of a Minesweeper Game.
    /// </summary>
    public class Game : IReadOnlyDictionary<BlockCoordinates, BlockState>
    {
        /// <summary>
        /// Represents the status of a Minesweeper Game.
        /// </summary>
        public enum GameStatus
        {
            /// <summary>
            /// The status of a defeated Game.
            /// </summary>
            /// <remarks>
            /// A defeated Game is defined as one which was completely revealed after trying to reveal a block with a mine.
            /// </remarks>
            Defeat = -1,

            /// <summary>
            /// The status of a Game in progress.
            /// </summary>
            /// <remarks>
            /// A game in progress is defined as one wich contains at least one non-revealed and non-flagged block.
            /// </remarks>
            InProgress = 0,

            /// <summary>
            /// The status of a victorious Game
            /// </summary>
            /// <remarks>
            /// A victorious game is defined as one which was completely reavealed after revealing all blocks with no mines and flagging all blocks with mines.
            /// </remarks>
            Victory = 1,
        }

        private readonly GameBoard gameBoard;

        private Game(int width, int height)
        {
            if (width < 1)
                throw new ArgumentOutOfRangeException(nameof(width), width, "The width of a Game cannot be less than 1.");
            if (height < 1)
                throw new ArgumentOutOfRangeException(nameof(width), width, "The width of a Game cannot be less than 1.");

            gameBoard = new GameBoard(width, height);
        }

        /// <summary>
        /// Creates an instance of a Game with random mine coordinates.
        /// </summary>
        /// <param name="width">The number of columns of blocks in the Game.</param>
        /// <param name="height">The number of rows of blocks in the Game.</param>
        /// <param name="mineCount">The number blocks with a mine in the Game.</param>
        /// <param name="random">The pseudo-random number generator.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="width"/> or <paramref name="height"/> are less than 1, or if <paramref name="mineCount"/> is less than 0.</exception>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="random"/> is null.</exception>"
        /// <exception cref="ArgumentException">Thrown if <paramref name="mineCount"/> is greater than <paramref name="width"/> times <paramref name="height"/>.</exception>
        public Game(int width, int height, int mineCount, Random random)
            : this(width, height)
        {
            if (mineCount < 0)
                throw new ArgumentOutOfRangeException(nameof(mineCount), mineCount, "The number of blocks with a mine in a Game cannot be less than 0.");
            if (mineCount > Size)
                throw new ArgumentException("The number of blocks with a mine in a Game cannot be greater than its size.", nameof(mineCount));
            if (random is null)
                throw new ArgumentNullException(nameof(random));

            for (int minesPlaced = 0; minesPlaced < mineCount; minesPlaced++)
            {
                int x = random.Next(width);
                int y = random.Next(height);
                var c = new BlockCoordinates(x, y);

                while (gameBoard[c].HasMine)
                {
                    x = (x + 1) % width;
                    if (x == 0)
                        y = (y + 1) % height;

                    c = new BlockCoordinates(x, y);
                }

                gameBoard[c].HasMine = true;
            }
        }

        /// <summary>
        /// Creates an instance of a Game with known mine coordinates.
        /// </summary>
        /// <param name="width">The number of columns of blocks in the Game.</param>
        /// <param name="height">The number of rows of blocks in the Game.</param>
        /// <param name="mineCoordinates">The coordinates of the blocks with a mine in the Game.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="width"/> or <paramref name="height"/> are less than 1.</exception>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="mineCoordinates"/> is null.</exception>"
        /// <exception cref="ArgumentException">Thrown if any of the coordinates in <paramref name="mineCoordinates"/> is outside of the Game boundaries.</exception>
        public Game(int width, int height, IEnumerable<BlockCoordinates> mineCoordinates)
            : this(width, height)
        {
            if (mineCoordinates is null)
                throw new ArgumentNullException(nameof(mineCoordinates));

            foreach (BlockCoordinates c in mineCoordinates)
            {
                if (!gameBoard.TryGetValue(c, out Block b))
                    throw new ArgumentException("The coordinates of all blocks with a mine in a Game must be within its boundaries.", nameof(mineCoordinates));

                b.HasMine = true;
            }
        }

        /// <summary>
        /// Gets the number of columns of blocks in the Game.
        /// </summary>
        public int Width => gameBoard.Width;

        /// <summary>
        /// Gets the number of rows of blocks in the Game.
        /// </summary>
        public int Height => gameBoard.Height;

        /// <summary>
        /// Gets the number of blocks in the Game.
        /// </summary>
        public int Size => gameBoard.Count;

        /// <summary>
        /// Gets the number of blocks with a mine in the Game.
        /// </summary>
        public int MineCount => Blocks.Count(b => b.HasMine);

        /// <summary>
        /// Gets the number of blocks with a flag in the Game.
        /// </summary>
        public int FlagCount => Blocks.Count(b => b.HasFlag);

        /// <summary>
        /// Gets the status of the Game.
        /// </summary>
        public GameStatus Status
        {
            get
            {
                if (Blocks.Any(b => !b.IsRevealed))
                    return GameStatus.InProgress;

                if (Blocks.Any(b => b.HasMine != b.HasFlag))
                    return GameStatus.Defeat;

                 return GameStatus.Victory;
            }
        }

        /// <summary>
        /// Gets an enumerable collection that contains all valid coordinates in the Game.
        /// </summary>
        public IEnumerable<BlockCoordinates> BlockCoordinates => gameBoard.Keys;

        /// <summary>
        /// Gets an enumerable collection that contains the state of all blocks in the Game.
        /// </summary>
        public IEnumerable<BlockState> BlockStates => BlockCoordinates.Select(c => this[c]);

        private IEnumerable<Block> Blocks => gameBoard.Values;

        /// <summary>
        /// Gets the state of a block in the Game.
        /// </summary>
        /// <param name="c">The coordinates of the block.</param>
        /// <returns>The state of the block in the specified coordinates.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="c"/> is outside of the Game boundaries.</exception>
        public BlockState this[BlockCoordinates c]
        {
            get
            {
                if (!TryGetBlockState(c, out BlockState state))
                    throw new ArgumentOutOfRangeException(nameof(c), c, "The coordinates must be within the Game boundaries.");

                return state;
            }
        }

        /// <summary>
        /// Reveals the state of a block in the Game.
        /// </summary>
        /// <param name="c">The coordinates of the block.</param>
        /// <returns>The new state of the block after revealing it.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="c"/> is outside of the Game boundaries.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the block in the specified coordinates is flagged.</exception>
        /// <remarks>
        /// If the block is already revealed, no operation is performed.
        /// If the block is flagged, an <code>InvalidOperationException</code> is thrown.
        /// If the block contains a mine, all the blocks in the Game will be revealed and the Game will result in a defeat.
        /// If the block does not contain a mine, the count of mines in the surrounding blocks will be revealed.
        /// If the count of surrounding mines is zero, all surrounding non-flagged blocks are recursively revealed.
        /// </remarks>
        public BlockState Reveal(BlockCoordinates c)
        {
            if (!gameBoard.TryGetValue(c, out Block b))
                throw new ArgumentOutOfRangeException(nameof(c), c, "The coordinates must be within the Game boundaries.");
            if (b.HasFlag)
                throw new InvalidOperationException("The block in the specified coordinates cannot be revealed because it is flagged.");

            if (!b.IsRevealed)
            {
                b.IsRevealed = true;

                if (b.HasMine)
                {
                    foreach(Block bb in Blocks)
                    {
                        bb.IsRevealed = true;
                    }
                }
                else
                {
                    IEnumerable<BlockCoordinates> surroundingCoordinates = GetSurroundingCoordinates(c);
                    int surroundingMineCount = surroundingCoordinates.Count(cc => gameBoard[cc].HasMine);
                    if (surroundingMineCount == 0)
                    {
                        foreach(BlockCoordinates cc in surroundingCoordinates)
                        {
                            Block bb = gameBoard[cc];
                            if (!bb.IsRevealed && !bb.HasFlag)
                            {
                                Reveal(cc);
                            }
                        }
                    }
                }
            }

            return this[c];
        }

        /// <summary>
        /// Sets or removes a flag from a block in the Game.
        /// </summary>
        /// <param name="c">The coordiantes of the block.</param>
        /// <param name="flag">A boolean value indicating whether to set or remove the flag from the block.</param>
        /// <returns>The new state of the block after setting or removing the flag.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="c"/> is outside of the Game boundaries.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the block in the specified coordinates is revealed.</exception>
        public BlockState SetFlag(BlockCoordinates c, bool flag)
        {
            if (!gameBoard.TryGetValue(c, out Block b))
                throw new ArgumentOutOfRangeException(nameof(c), c, "The coordinates must be within the Game boundaries.");
            if (b.IsRevealed)
                throw new InvalidOperationException("The block in the specified coordinates cannot be flagged because it is revealed.");

            b.HasFlag = flag;

            return this[c];
        }

        /// <summary>
        /// Checks whether the specified coordinates are within the Game boundaries.
        /// </summary>
        /// <param name="c">The coordinates.</param>
        /// <returns>A boolean value indicating whether the specified coordiantes are within the Game boundaries or not.</returns>
        public bool ContainsCoordinates(BlockCoordinates c)
        {
            return gameBoard.ContainsKey(c);
        }

        /// <summary>
        /// Gets the state of a block in the Game.
        /// </summary>
        /// <param name="c">The coordinates of the block.</param>
        /// <param name="state">The state of the block in the specified coordinates, or a default value if the coordinates are not within the Game boundaries.</param>
        /// <returns>A boolean value indicating whether the state of the block could be retrieved succesfully or not.</returns>
        public bool TryGetBlockState(BlockCoordinates c, out BlockState state)
        {
            state = default;

            if (!gameBoard.TryGetValue(c, out Block b))
                return false;

            if (!b.IsRevealed)
            {
                state = BlockState.NonRevealed(b.HasFlag);
            }
            else
            {
                if (!b.HasMine)
                {
                    int surroundingMineCount = GetSurroundingCoordinates(c).Count(cc => gameBoard[cc].HasMine);
                    state = BlockState.RevealedWithNoMine(b.HasFlag, surroundingMineCount);
                }
                else
                {
                    state = BlockState.RevealedWithMine(b.HasFlag);
                }
            }

            return true;
        }

        private IEnumerable<BlockCoordinates> GetSurroundingCoordinates(BlockCoordinates c)
        {
            int minX = c.X - 1;
            int maxX = c.X + 1;
            int minY = c.Y - 1;
            int maxY = c.Y + 1;

            minX = Math.Max(0, minX);
            maxX = Math.Min(Width - 1, maxX);
            minY = Math.Max(0, minY);
            maxY = Math.Min(Height - 1, maxY);

            int countX = maxX - minX + 1;
            int countY = maxY - minY + 1;

            return Enumerable.Range(minX, countX)
                .SelectMany(x =>
                {
                    return Enumerable.Range(minY, countY)
                        .Select(y => new BlockCoordinates(x, y));
                })
                .Except(new BlockCoordinates[] { c });
        }

        private IEnumerator<KeyValuePair<BlockCoordinates, BlockState>> GetEnumerator()
        {
            return BlockCoordinates
                .Select(c => new KeyValuePair<BlockCoordinates, BlockState>(c, this[c]))
                .GetEnumerator();
        }

        #region IReadOnlyDictionary Implenetation
        IEnumerable<BlockCoordinates> IReadOnlyDictionary<BlockCoordinates, BlockState>.Keys => BlockCoordinates;

        IEnumerable<BlockState> IReadOnlyDictionary<BlockCoordinates, BlockState>.Values => BlockStates;

        int IReadOnlyCollection<KeyValuePair<BlockCoordinates, BlockState>>.Count => Size;

        bool IReadOnlyDictionary<BlockCoordinates, BlockState>.ContainsKey(BlockCoordinates key)
        {
            return ContainsCoordinates(key);
        }

        bool IReadOnlyDictionary<BlockCoordinates, BlockState>.TryGetValue(BlockCoordinates key, out BlockState value)
        {
            return TryGetBlockState(key, out value);
        }

        IEnumerator<KeyValuePair<BlockCoordinates, BlockState>> IEnumerable<KeyValuePair<BlockCoordinates, BlockState>>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion

        private class GameBoard : ReadOnlyDictionary<BlockCoordinates, Block>
        {
            public GameBoard(int width, int height)
                : base(Create(width, height))
            {
                Width = width;
                Height = height;
            }

            public int Width { get; }

            public int Height { get; }

            private static IDictionary<BlockCoordinates, Block> Create(int width, int height)
            {
                IEnumerable<BlockCoordinates> allCoordinates = Enumerable.Range(0, width)
                    .SelectMany(x =>
                    {
                        return Enumerable.Range(0, height)
                            .Select(y => new BlockCoordinates(x, y));
                    });

                return allCoordinates
                     .ToDictionary(
                         keySelector: c => c,
                         elementSelector: c => new Block());
            }
        }

        private class Block
        {
            public bool HasMine { get; set; }

            public bool HasFlag { get; set; }

            public bool IsRevealed { get; set; }
        }
    }
}
