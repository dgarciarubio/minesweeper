﻿using System;

namespace Minesweeper
{
    /// <summary>
    /// Represents the coordinates of a block in the Game.
    /// </summary>
    public struct BlockCoordinates : IEquatable<BlockCoordinates>
    {
        /// <summary>
        /// Creates a new BlockCoordiantes value for the specified x and y components.
        /// </summary>
        /// <param name="x">The zero-based column index of the block.</param>
        /// <param name="y">The zero-based row index of the block.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="x"/> or <paramref name="y"/> are less than 0.</exception>
        public BlockCoordinates(int x, int y)
        {
            if (x < 0)
                throw new ArgumentOutOfRangeException(nameof(x), x, "The x component of a BlockCoordinates value cannot be less than 0.");
            if (y < 0)
                throw new ArgumentOutOfRangeException(nameof(y), y, "The y component of a BlockCoordinates value cannot be less than 0.");

            X = x;
            Y = y;
        }

        /// <summary>
        /// Gets the zero-based column index of the block.
        /// </summary>
        public int X { get; }
        /// <summary>
        /// Gets the zero-based row index of the block.
        /// </summary>
        public int Y { get; }

        /// <summary>
        /// Indicates whether this coordinates and another coordinates specified are equal.
        /// </summary>
        /// <param name="other">The coordinates to compare with the current instance.</param>
        /// <returns>A value indicating whether this instance and the specified coordinates are equal.</returns>
        public bool Equals(BlockCoordinates other)
        {
            return X == other.X && Y == other.Y;
        }

        /// <summary>
        /// Indicates whether this instance and the specified object are equal.
        /// </summary>
        /// <param name="obj">The object to compare with the current instance.</param>
        /// <returns>A value indicating whether this instance and the specified object are equal.</returns>
        public override bool Equals(object obj)
        {
            if (obj is null)
                return false;
            if (!(obj is BlockCoordinates objCoordinates))
                return false;

            return Equals(objCoordinates);
        }

        /// <summary>
        /// Returns the hashcode for these coordinates.
        /// </summary>
        /// <returns>A 32-bit signed integer that is the hash code for these coordinates.</returns>
        public override int GetHashCode()
        {
            int hashCode = 31;

            hashCode = (hashCode * 23) + X.GetHashCode();
            hashCode = (hashCode * 23) + Y.GetHashCode();

            return hashCode;
        }

        /// <summary>
        /// Converts the value of these coordinates to its equivalent string representation.
        /// </summary>
        /// <returns>The string representation for these coordinates.</returns>
        public override string ToString()
        {
            return ToString(null, null);
        }

        /// <summary>
        /// Converts the value of these coordinates to their equivalent string representation using the specified culture-specific format information.
        /// </summary>
        /// <param name="provider">An object that supplies culture-specific formatting information.</param>
        /// <returns>The string representation for these coordinates as specified by <paramref name="provider"/>.</returns>
        public string ToString(IFormatProvider provider)
        {
            return ToString(null, provider);
        }

        /// <summary>
        /// Converts the value of these coordinates to their equivalent string representation using the specified format.
        /// </summary>
        /// <param name="format"> A standard or custom numeric format string.</param>
        /// <returns>The string representation for these coordinates as specified by <paramref name="format"/>.</returns>
        public string ToString(string format)
        {
            return ToString(format, null);
        }

        /// <summary>
        /// Converts the value of these coordinates to their equivalent string representation using the specified format and culture-specific format information.
        /// </summary>
        /// <param name="format"> A standard or custom numeric format string.</param>
        /// <param name="provider">An object that supplies culture-specific formatting information.</param>
        /// <returns>The string representation for these coordinates as specified by <paramref name="format"/> and <paramref name="provider"/>.</returns>
        public string ToString(string format, IFormatProvider provider)
        {
            string formattedX = X.ToString(format, provider);
            string formattedY = Y.ToString(format, provider);

            return $"({formattedX} {formattedY})";
        }

        /// <summary>
        /// Indicates whether a pair of coordinates are equal.
        /// </summary>
        /// <param name="left">The first element of the coordinates pair.</param>
        /// <param name="right">The second element of the coordinates pair.</param>
        /// <returns>A value indicating whether the specified pair of coordinates are equal.</returns>
        public static bool operator == (BlockCoordinates left, BlockCoordinates right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Indicates whether a pair of coordinates are not equal.
        /// </summary>
        /// <param name="left">The first element of the coordinates pair.</param>
        /// <param name="right">The second element of the coordinates pair.</param>
        /// <returns>A value indicating whether the specified pair of coordinates are not equal.</returns>
        public static bool operator != (BlockCoordinates left, BlockCoordinates right)
        {
            return !left.Equals(right);
        }
    }
}